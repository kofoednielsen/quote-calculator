# deps
from flask import Flask
from flask.json import jsonify
from loguru import logger

# app
from parser import parse_excel



app = Flask(__name__, static_folder="/static")


@app.route("/")
def index():
    logger.info("Serving index.html")
    return app.send_static_file("index.html")


@app.route("/data")
def data():
    logger.info("Parsing and serving data")
    return jsonify(parse_excel())
