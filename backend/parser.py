# std imports
from uuid import uuid4
import math

# deps
from pandas import read_excel
import numpy as np
from loguru import logger


def parse_excel():
    """ Parse excel file into a json object, prices are folder into a child objects """
    data = read_excel("/tilbud.xlsx")

    # remove empty rows
    dropped_na = data[~data["Category"].isnull()]
    # replace nan with empty string, as nan cant be json serialized
    na_replaced = dropped_na.replace(np.nan, "", regex=True)
    # make groups, the groups are products, and the items
    # in the grups, are the pricings for the product!
    groups = na_replaced.groupby(
        [
            "Category",
            "Subcategory",
            "SubSubcategory",
            "SubSubSubCategory",
            "Enhed",
            "ERegnskab"
        ],
        dropna=False,
    )
    products = []
    for index, group in groups:
        product = {
            "category": index[0],
            "subcategory": index[1],
            "subsubcategory": index[2],
            "subsubsubcategory": index[3],
            "unit": index[4],
            "eregnskab": index[5],
            "prices": [],
        }
        for row in group[["Interval", "Enhedspris kr pr. enhed"]].itertuples():
            price = row[2]
            # TODO: fix this hack, we need consistent data in interval column
            # apparently an empty excel cell is a NaN float
            if isinstance(row[1], str) and row[1] == "":
                from_amount = 0
            elif isinstance(row[1], int):
                from_amount = row[1]
            elif "-" in row[1]:
                from_amount = int(row[1].split("-")[0])
            else:
                raise Exception(f"Wtf is this interval? {row[0]}")
            product["prices"].append(
                {"from_amount": from_amount, "price": price}
            )
        products.append(product)

    return products
