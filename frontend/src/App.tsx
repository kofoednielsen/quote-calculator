import React, { useEffect, useState } from "react";
import "./App.css";
import "antd/dist/antd.css";
import { Card, Breadcrumb, Form, Button, Input } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import { InputForm } from "./components/input-form";
import { saveAs } from "file-saver";
import useLocalStorageState from 'use-local-storage-state'
import xlsx from "node-xlsx";

export type Price = {
  from_amount: number;
  price: number;
};

export type Product = {
  category: string;
  subcategory: string;
  subsubcategory: string;
  subsubsubcategory: string;
  unit: string;
  eregnskab: string;
  prices: Array<Price>;
};

export type Order = {
  product: Product;
  amount: number;
};

export const App = (): JSX.Element => {
  const [products, setProducts] = useState<Array<Product>>([]);
  const [orders, setOrders] = useLocalStorageState<Array<Order>>("orders", []);
  const [filename, setFilename] = useState<string>("");

  const onSubmitOrder = (product: Product, amount: number) => {
    var order: Order = {
      product: product,
      amount: amount,
    };
    setOrders((orders) => [...orders, order]);
  };

  const onDeleteOrder = (order: Order) => {
    setOrders((orders) => orders.filter((o) => o !== order));
  };

  const exportXlsx = () => {
    // Format orders as xlsx file and save as a file

    const data = [
      ["Type", "", "Beskrivelse", "Antal", "Salgspris stk"],
      ...orders.map((o) => [
        "Vare",
        o.product.eregnskab,
        `${o.product.category} ${o.product.subcategory} ${o.product.subsubcategory} ${o.product.subsubsubcategory}`,
        o.amount,
        findUnitPriceForOrder(o),
      ]),
    ];
    var buffer = xlsx.build([{ name: filename, data: data }]); // Returns a buffer

    var blob = new Blob([buffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    saveAs(blob, filename);
  };

  const findUnitPriceForOrder = (order: Order) => {
    // Find all prices smaller than amount
    var tmp = order.product.prices.filter(
      (price) => price.from_amount < order.amount
    );
    // Find min
    const unit_price = tmp.reduce(
      (acc, price) => (price.price < acc ? (acc = price.price) : (acc = acc)),
      global.Infinity
    );
    return unit_price;
  };

  const calculateOrderPrice = (order: Order) => {
    // Find all prices smaller than amount
    return findUnitPriceForOrder(order) * order.amount;
  };

  useEffect(() => {
    fetch("/data")
      .then((response) => response.json())
      .then((data) => {
        console.log("Successs.", data);
        setProducts(data);
      });
  }, []);

  return (
    <div>
      <InputForm products={products} onSubmit={onSubmitOrder} />
      {orders.map((order) => (
        <Card
          actions={[<DeleteOutlined onClick={() => onDeleteOrder(order)} />]}
        >
          <Breadcrumb>
            <Breadcrumb.Item>{order.product.category}</Breadcrumb.Item>
            <Breadcrumb.Item>{order.product.subcategory}</Breadcrumb.Item>
            <Breadcrumb.Item>{order.product.subsubcategory}</Breadcrumb.Item>
            <Breadcrumb.Item>{order.product.subsubsubcategory}</Breadcrumb.Item>
          </Breadcrumb>
          <span>{order.amount + " " + order.product.unit}</span>
          <br />
          <span>{findUnitPriceForOrder(order)} dkk - enhedspris</span>
          <br />
          <span>{calculateOrderPrice(order)} dkk - subtotal</span>
        </Card>
      ))}
      <Card>
        <span>
          Total:{" "}
          {orders
            .map((o) => calculateOrderPrice(o))
            .reduce((a: number, b: number) => a + b, 0)}{" "}
          dkk
        </span>
      </Card>
      <Card>
        <Form>
          <Form.Item>
            <Input
              value={filename}
              onChange={(e) => setFilename(e.target.value)}
              placeholder="filnavn.."
            />
          </Form.Item>
          <Form.Item>
            <Button
              disabled={filename === ""}
              type="primary"
              onClick={() => exportXlsx()}
            >
              Exporter som excel fil
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};
