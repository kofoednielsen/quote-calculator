import React, { useState, useEffect } from 'react'
import { Menu, Dropdown, Button, Card, InputNumber, Form, Breadcrumb } from 'antd'
import { Product } from '../App'


type props = {
    products: Array<Product>;
    onSubmit: (product: Product, amount: number) => void;
}

export const InputForm = ({onSubmit, products}: props): JSX.Element => {

  const [product, setProduct] = useState<Product | null>(null)
  const [amount, setAmount] = useState<number>(0)
  const [menu, setMenu] = useState<JSX.Element>(<></>)

  var _ = require('lodash');
  const { SubMenu } = Menu;

  const submit = () => {
    if (product != null)
        onSubmit(product, amount)
        setProduct(null)
        setAmount(0)
  }

  const renderSubMenu = (category_depth: string, category_names: any, categories: any): JSX.Element => {
    // for ever category
    return category_names.map((category_name: string) => {
      // Decide if we are at a leaf
      var have_subcategories = true
      if (categories[category_name].length === 1)
        have_subcategories = !(categories[category_name][0]["sub" + category_depth] === ""  || 
                               categories[category_name][0]["sub" + category_depth] === undefined )
      if (have_subcategories) {
        var subcategories = _.groupBy(categories[category_name], "sub" + category_depth)
        var subcategory_names = Object.keys(subcategories)
        return (
          <SubMenu key={category_name} title={category_name}>
            {renderSubMenu("sub" + category_depth, subcategory_names, subcategories)}
          </SubMenu>
        )
      } else {
        return (
          <Menu.Item key={category_name} onClick={() => setProduct(categories[category_name][0])}>
            {category_name}
          </Menu.Item>
      )}
    })}
  
  useEffect(() => {
    var categories = _.groupBy(products, "category")
    var category_names = Object.keys(categories)
    setMenu(
      <Menu>
        {renderSubMenu("category", category_names, categories)}
      </Menu>
    )
  }, [products])

     
  var product_name = <a>Vælg Produkt</a>
  if (product != null)
    product_name = <>
        <Breadcrumb>
            <Breadcrumb.Item>
              {product.category}
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              {product.subcategory}
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              {product.subsubcategory}
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              {product.subsubsubcategory}
            </Breadcrumb.Item>
         </Breadcrumb>
      </>
  return (
    <Card>
      <Form
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 8 }}
        layout="horizontal"
      >
        <Form.Item>
          <Dropdown overlay={menu}>
            <Button>
              {product_name}
           </Button>
          </Dropdown>
        </Form.Item>
        <Form.Item>
          <InputNumber value={amount} defaultValue={0} onChange={(value: any) => setAmount(value)}/>
        </Form.Item>
        <Form.Item>
          <Button disabled={product == null || amount < 1} type="primary" onClick={() => submit()}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Card>
  )
}
