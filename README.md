# Quote Calculator

Web app for calculating quotes with a spreadsheet as datasource


# Running

To run full development environment with hot-reloading, put a `tilbud.xlsx` file in the project directory
and then run
> Make sure you have docker and docker-compose installed
```
docker-compose up --build
```
