# ░█▀▀░█▀▄░█▀█░█▀█░▀█▀░█▀▀░█▀█░█▀▄
# ░█▀▀░█▀▄░█░█░█░█░░█░░█▀▀░█░█░█░█
# ░▀░░░▀░▀░▀▀▀░▀░▀░░▀░░▀▀▀░▀░▀░▀▀░
FROM node:13 as frontend

RUN mkdir /tmp/frontend_build/

# install frontend deps
COPY ./frontend/package.json /tmp/frontend_build/package.json
COPY ./frontend/package-lock.json /tmp/frontend_build/package-lock.json
WORKDIR /tmp/frontend_build
RUN npm install

# build frontend
COPY ./frontend /tmp/frontend_build

RUN npm run-script build

# ░█▀▄░█▀█░█▀▀░█░█░█▀▀░█▀█░█▀▄
# ░█▀▄░█▀█░█░░░█▀▄░█▀▀░█░█░█░█
# ░▀▀░░▀░▀░▀▀▀░▀░▀░▀▀▀░▀░▀░▀▀░
FROM python:3.8-slim-buster

RUN mkdir /app

# install deps
COPY backend/requirements.txt /app/
RUN pip install -r /app/requirements.txt

# sauce
COPY backend/app.py /app/
COPY backend/parser.py /app/

# copy frontend in
COPY --from=frontend /tmp/frontend_build/build/static/ /static/
COPY --from=frontend /tmp/frontend_build/build/index.html /static/index.html
COPY --from=frontend /tmp/frontend_build/build/manifest.json /static/manifest.json

# run Forest, run!
WORKDIR /app
CMD gunicorn --bind "0.0.0.0:80" app:app
